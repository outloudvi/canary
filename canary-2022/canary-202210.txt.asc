-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

WARRANT CANARY
====================================
Last updated       : 2022/10/02
Next update before : 2022/11/04
====================================
What is a warrant canary: https://en.wikipedia.org/wiki/Warrant_canary
====================================

Issued for 2022/10.

Don't just trust the contents of this file blindly! Verify the
digital signatures! Also, make sure the keys are correct!

Signers
~~~~~~~~~~~~~

Outvi V: 0xFF62255C1087F21C
 (Main key: 0xA725CB57CA65CAFE)

Statements
~~~~~~~~~~~~~

1. All our infrastructure is in our control, the integrity of our
entire system is sound.

2. We have not been compromised or suffered a recent data breach,
to our best knowledge.

3. We have not disclosed any private encryption keys.

4. We have not been forced to modify our system to allow access or
information leakage to a third party.

5. We haven't received any specific orders, requests or recommendations
from any authorities, whether formal or informal.

6. We have not received any court orders, gag orders, or other similar
orders from the government of the People's Republic of China.

7. We have not received any government subpoenas.

8. Our personal safety and security is not threatened.

9. We plan to publish the next of these canary statements before 2022/11/04.
Special note should be taken if no new canary is published by that time
or if the list of statements changes without plausible explanation.

10. We still have access to the "real" Internet, e.g. New York Times.

Special Announcements
~~~~~~~~~~~~~~~~~~~~~~~~

Note that the rsstail issue has been fixed.

Proof of Freshness
~~~~~~~~~~~~~~~~~~~~

$ rsstail -n5 -u https://www.telegraph.co.uk/news/rss.xml -l -N -1
 Sunday morning UK news briefing: Today's top headlines from The Telegraph
 https://www.telegraph.co.uk/news/2022/10/02/sunday-morning-uk-news-briefing-todays-top-headlines-telegraph/
 Olivia Pratt-Korbel: Man charged with murder of nine-year-old
 https://www.telegraph.co.uk/news/2022/10/01/olivia-pratt-korbel-man-charged-murder-nine-year-old/
 Train strikes: Worst rail disruption of the year grinds network to a halt
 https://www.telegraph.co.uk/news/2022/10/01/train-strikes-worst-rail-disruption-year-grinds-network-halt/
 ‘Dental deserts’ leave just one NHS dentist per 16,000 people in parts of country
 https://www.telegraph.co.uk/news/2022/10/01/dental-deserts-leave-just-one-nhs-dentist-per-16000-people-parts/
 Mark Mardell reveals he has Parkinson’s disease
 https://www.telegraph.co.uk/news/2022/10/01/mark-mardell-reveals-has-parkinsons-disease/

$ rsstail -n5 -u https://rss.nytimes.com/services/xml/rss/nyt/World.xml -l -N -1
 Live Updates: Death Toll Rises After Indonesian Soccer Match
 https://www.nytimes.com/live/2022/10/02/world/indonesia-soccer-football
 In Washington, Putin’s Nuclear Threats Stir Growing Alarm
 https://www.nytimes.com/2022/10/01/world/europe/washington-putin-nuclear-threats.html
 Panic, Bribes, Ditched Cars and a Dash on Foot: Portraits of Flight From Russia
 https://www.nytimes.com/2022/10/01/world/europe/escape-from-russia.html
 Zelensky Says Russian Retreat Shows Annexation Is a ‘Farce’
 https://www.nytimes.com/live/2022/10/02/world/russia-ukraine-war-news
 ‘Naked Fear’: A Pipeline Attack Fans Anxiety in a German Village, and Beyond
 https://www.nytimes.com/2022/10/01/world/europe/germany-pipeline-damage-fear.html

$ date -R -u
Sun, 02 Oct 2022 08:33:49 +0000

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEepVDgp5HXX04JrCNpyXLV8plyv4FAmM5TO8ACgkQpyXLV8pl
yv6Piw//U86VXOLV4jrNmAERrgvDYJibyOwAyfuKEPK5kpqfyNrPFZ1BjMKRSKZu
jmmAtierQ9xlD7l+1IWbPQp+rtJU0kV7sl+Px3QlDy69v1pIkifeTV3QbHRSTdst
Ni0I/f5iC50pe+ZLP+yCSMFURO8sysf+glmBn3TLgBNmRHtCsZQdpQbXvXgvC4hN
Q8DmuBVXIlz6yMiczLWDIxhb2ruvxcug6YAj+ni4vFDDgTcFcaL+UYM3Nu19rahC
tUoCoyuPQ38/sovm3RU1G9MxWqsqM3RWOjuafoCBXZQdK5xBCVNKDa3MpQ1dGPW8
Y/u16HOiIFNIs/KOZ8fj+PmfnSFNfVbqoMeHvt/gNshWaSMAQcXPw10CO+0n0Q2y
yDD1V9voQ07KkPl4glTCj3WwJycqSoo7VpqlwpCmT/HtoD1Vmgp6jEKcjUaSxtVH
JHePidvciv91+OTn67PLeilVuZFw4HSXJLpnb7RmYSbmXYuu8ydKoxhVIhnmRLVy
aXrf+dsGzJ1LB10MvlUIMskX0070sXnrQVqf1EqVQweyoH8e2ou+GHsOEbXY6TDy
t1hD/4azZHFGosKoMSkJfhrzuUq+zwDY5ojd8kwfSlT7H9kxZeQ68kzvyEI4/DXA
UhFgexw8H908QltBycCinGikpFhoP1KMt5sMlADJ/rAY5xhGGL0=
=ZZCN
-----END PGP SIGNATURE-----
