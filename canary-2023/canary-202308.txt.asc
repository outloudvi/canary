-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

WARRANT CANARY
====================================
Last updated       : 2023/08/01
Next update before : 2023/09/03
====================================
What is a warrant canary: https://en.wikipedia.org/wiki/Warrant_canary
====================================

Issued for 2023/08.

Don't just trust the contents of this file blindly! Verify the
digital signatures! Also, make sure the keys are correct!

Signers
~~~~~~~~~~~~~

Outvi V: 0xFF62255C1087F21C
 (Main key: 0xA725CB57CA65CAFE)

Statements
~~~~~~~~~~~~~

1. All our infrastructure is in our control, the integrity of our
entire system is sound.

2. We have not been compromised or suffered a recent data breach,
to our best knowledge.

3. We have not disclosed any private encryption keys.

4. We have not been forced to modify our system to allow access or
information leakage to a third party.

5. We haven't received any specific orders, requests or recommendations
from any authorities, whether formal or informal.

6. We have not received any court orders, gag orders, or other similar
orders from the government of the People's Republic of China.

7. We have not received any government subpoenas.

8. Our personal safety and security is not threatened.

9. We plan to publish the next of these canary statements before 2023/09/03.
Special note should be taken if no new canary is published by that time
or if the list of statements changes without plausible explanation.

10. We still have access to the "real" Internet, e.g. New York Times.

Special Announcements
~~~~~~~~~~~~~~~~~~~~~~~~



Proof of Freshness
~~~~~~~~~~~~~~~~~~~~

$ rsstail -n5 -u https://www.telegraph.co.uk/news/rss.xml -l -N -1
 Captain Sir Tom Moore's family defend unauthorised home spa at risk of demolition
 https://www.telegraph.co.uk/news/2023/08/01/captain-tom-moore-family-daughter-defend-spa-building/
 Debut novelists make Booker Prize 2023 longlist as star writers miss out
 https://www.telegraph.co.uk/news/2023/08/01/booker-prize-2023-longlist-viktoria-lloyd-barlow/
 Lockdown ‘harmed emotional development of almost half of children’
 https://www.telegraph.co.uk/news/2023/08/01/lockdown-harmed-emotional-development-almost-half-children/
 Monday evening news briefing: Rishi Sunak grants hundreds of new North Sea oil and gas licences
 https://www.telegraph.co.uk/news/2023/07/31/monday-evening-news-briefing-rishi-sunak-grants-hundreds-of/
 Ukraine: The Latest - "Russia has properly transitioned onto the defensive"
 https://www.telegraph.co.uk/news/2023/07/31/russia-has-properly-transitioned-onto-the-defensive/

$ rsstail -n5 -u https://rss.nytimes.com/services/xml/rss/nyt/World.xml -l -N -1
 Russia-Ukraine War: Drone Again Hits a Moscow Building Housing Russian Ministries
 https://www.nytimes.com/live/2023/08/01/world/russia-ukraine-news
 Niger Coup Is Backed by 2 Neighbors, Raising Risk of Wider Conflict
 https://www.nytimes.com/2023/08/01/world/africa/niger-coup-mali-burkina-faso.html
 UNESCO Recommends Venice for Endangered List
 https://www.nytimes.com/2023/08/01/world/europe/venice-unesco-world-heritage-danger-list.html
 Construction to Break German Dependence on Russian Gas Hits a Snag: Wartime Bombs
 https://www.nytimes.com/2023/08/01/world/europe/germany-gas-terminal-munitions.html
 Russia’s Law Targeting Transgender People Sows Fear for L.G.B.T. Russians
 https://www.nytimes.com/2023/08/01/world/europe/russia-transgender-ban.html

$ date -R -u
Tue, 01 Aug 2023 14:51:25 +0000

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEepVDgp5HXX04JrCNpyXLV8plyv4FAmTJG+0ACgkQpyXLV8pl
yv61vQ//dHfKtXUCQPxX2X+UMjNNw35VU96TtZRg1NQRWBSY1fUUJdS9Lg7qC53N
p2T6SEUEOZO80JPKpfLSU0MYQviFZ1C9dZnU19vgHpb1AoABLUxT2PEu4gdVEV+N
jlJpJnTJPbA8M0l3qVllQiH85BhLccq7ZAF7li18SQoNu8V0oWEFzoOihfpvtioK
cCBC+p9bcIuz7Un1n7qdihAcL7scrrwABurFYMeD0iftw7zcWZ3E7Rr67mr3IJXB
8f73mPpPYJRKWGXXEbZRJCFgraLwqn8fJxswmmDsxiNQN/pOr+zGkTiI8YQFAR3F
xgNn7bTLiJUKMVOhtRdkVKCaEc+b6pZyh7u8tOHVXNvWTsw4G20DpsV2OHot5DpD
hcZt6ucvV+nk1Q0DZDWTKrplcXEqsagx6HRf3IXcoCg9QHTPZNEvJzFjAUcAcr9H
DQ4wTCHZnuHjNx5RBiaojU6SUAnNZQz+madeABRNUhXgtZsdXfOiWkbfwCVo1zcT
KIAHrF0NfTxFU8xvgjjxNU+7NxnrYtmDruKrBjDNsiukD9oNOMQBNNzftCr+MO4a
lx0WZRXSuEz0ly9Vd3fdVBlClTLVUxcbHOTPJ5LiFPjJVL3escN/6HRROe2MK/cc
JpkOGIwFuypgS6EnqJyuENn2TPZV5FQBBDjnh8wimpVX3XR85q4=
=hjJW
-----END PGP SIGNATURE-----
