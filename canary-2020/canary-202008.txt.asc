-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

WARRANT CANARY
====================================
Last updated       : 2020/08/08
Next update before : 2020/09/10
====================================
What is a warrant canary: https://en.wikipedia.org/wiki/Warrant_canary
====================================

Issued for 2020/08.

Don't just trust the contents of this file blindly! Verify the
digital signatures! Also, make sure the keys are correct!

Signers
~~~~~~~~~~~~~

Outvi V: 0xFF62255C1087F21C
 (Main key: 0xA725CB57CA65CAFE)

Statements
~~~~~~~~~~~~~

1. All our infrastructure is in our control, the integrity of our
entire system is sound.

2. We have not been compromised or suffered a recent data breach,
to our best knowledge.

3. We have not disclosed any private encryption keys.

4. We have not been forced to modify our system to allow access or
information leakage to a third party.

5. We haven't received any specific orders, requests or recommendations
from any authorities, whether formal or informal.

6. We have not received any court orders, gag orders, or other similar
orders from the government of the People's Republic of China.

7. We have not received any government subpoenas.

8. Our personal safety and security is not threatened.

9. We plan to publish the next of these canary statements before 2020/09/10.
Special note should be taken if no new canary is published by that time
or if the list of statements changes without plausible explanation.

10. We still have access to the "real" Internet, e.g. New York Times.

Special Announcements
~~~~~~~~~~~~~~~~~~~~~~~~

Farewell, Tsukasa Ria v1.

Proof of Freshness
~~~~~~~~~~~~~~~~~~~~

$ rsstail -e1 -n5 https://www.telegraph.co.uk/news/rss.xml -l -g
How many coronavirus cases are in your area? Use our tool to find out  
Coronavirus latest news: Preston told to bring in new lockdown restrictions following spike in cases  
How many coronavirus cases are in the UK - and where are they?  
What are the local lockdown rules in Preston, Aberdeen, Manchester, Kirklees and Bradford?  
Air India Express flight from Dubai crashes at Calicut airport, killing 14  

$ rsstail -e1 -n5 https://rss.nytimes.com/services/xml/rss/nyt/World.xml -l -g
Beijing Launches Another Demolition Drive, This Time in Its Bucolic Suburbs  
Sri Lanka Election Hands Rajapaksa Family a Bigger Slice of Control  
Aleksandr Lukashenko Increasingly in Peril as Belarus Election Nears  
For P.O.W., Landmark Verdict Against North Korea Is Long-Overdue Justice  
After a Lull, the Number of Migrants Trying to Enter the U.S. Has Soared  

$ date -R -u
Fri, 07 Aug 2020 17:20:54 +0000

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCgAdFiEEKMPlAfAJRhjEZCdo/2IlXBCH8hwFAl8tjXYACgkQ/2IlXBCH
8hz8GQ/+PdTf0vRyyJa1T6014S9wSgXqoMg9wKuNsQk19mIQFoL3OfGmwQ89EksL
n5Q0RFqkwp6+hxwsy51r3P5gZ8cIJWxmAOv8VsbcabG3Ll9j97/Ww9DuMzqywQvv
YLu6cKmNyyBujD4nWoFXsc1Jo1HTuy5OQa4e8NKkBgkzpudKHTjRIwF84vz+E5qi
mzfBvlqle+TvDppjnZhm7dyTgFjsz6O9WRQUakh77XLlW5ezh/mcgJ/XVjEjYk7a
bPa4KkbldpZRaQpBWkb1qUFZlUmr8M9wdIe9LKYiqOUfDf2vYsyZzJQZOxMw5u0l
jrKtLoHIbtxQUdrk0mdacDiKw0j4JU2HxhqSeRf7IqVo/phgofiyMpUjNafJlDxo
83OYIonVgIQsSAf5TvfPDjy1FnLSNDiKCkovjBWHf8t1JTwUusqZURebdRPKYO9h
Ul5YK0c+aiGQm1P7MCZPsj7wyWANCjythSRehoV/c6LXDDhLA3iAhRBe7alKJVfw
gF+p6NSPlFVuzX50v4uY+2QkaH7csKSTceADaW+bJzoW50uXEcc9GlaLNCtKHex+
Rew7g86bIUwrl+/xPvVBxAi31MnV+NYHt5GWuh2hpNgTQYiybzqffKn4kVS1b4NG
oyJNfnWFubni/7Bh9z9xjcQ9ONGfaPCCe1MxcrDQXVzTMDoS62Y=
=7qes
-----END PGP SIGNATURE-----
