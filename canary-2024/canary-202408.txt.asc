-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

WARRANT CANARY
====================================
Last updated       : 2024/08/02
Next update before : 2024/09/04
====================================
What is a warrant canary: https://en.wikipedia.org/wiki/Warrant_canary
====================================

Issued for 2024/08.

Don't just trust the contents of this file blindly! Verify the
digital signatures! Also, make sure the keys are correct!

Signers
~~~~~~~~~~~~~

Outvi V: 0xFF62255C1087F21C
 (Main key: 0xA725CB57CA65CAFE)

Statements
~~~~~~~~~~~~~

1. All our infrastructure is in our control, the integrity of our
entire system is sound.

2. We have not been compromised or suffered a recent data breach,
to our best knowledge.

3. We have not disclosed any private encryption keys.

4. We have not been forced to modify our system to allow access or
information leakage to a third party.

5. We haven't received any specific orders, requests or recommendations
from any authorities, whether formal or informal.

6. We have not received any court orders, gag orders, or other similar
orders from the government of the People's Republic of China.

7. We have not received any government subpoenas.

8. Our personal safety and security is not threatened.

9. We plan to publish the next of these canary statements before 2024/09/04.
Special note should be taken if no new canary is published by that time
or if the list of statements changes without plausible explanation.

10. We still have access to the "real" Internet, e.g. New York Times.

Special Announcements
~~~~~~~~~~~~~~~~~~~~~~~~



Proof of Freshness
~~~~~~~~~~~~~~~~~~~~

$ rsstail -n5 -u https://www.telegraph.co.uk/news/rss.xml -l -N -1
 Thursday evening news briefing: Starmer will not allow far-Right to co-ordinate ‘summer of riots’
 https://www.telegraph.co.uk/news/2024/08/01/thursday-evening-news-briefing-starmer-far-right-riots/
 Wednesday evening news briefing: Huw Edwards had indecent images of children ‘as young as seven’
 https://www.telegraph.co.uk/news/2024/07/31/wednesday-evening-news-briefing-huw-edwards-indecent-images/
 Tuesday evening news briefing: Families pay tribute to three girls killed in knife attack
 https://www.telegraph.co.uk/news/2024/07/30/tuesday-evening-news-briefing-southport-families-tribute/
 Missing six-year-old girl found safe and well
 https://www.telegraph.co.uk/news/2024/07/30/missing-girl-eudine-london-greenwich-estate/
 Watch: Knife-wielding ‘bus surfer’ filmed in Brighton
 https://www.telegraph.co.uk/news/2024/07/30/watch-knife-wielding-individual-bus-surfing-brighton/

$ rsstail -n5 -u https://rss.nytimes.com/services/xml/rss/nyt/World.xml -l -N -1
 Live Updates: Journalists and Dissidents Freed in Prisoner Exchange With Russia
 https://www.nytimes.com/live/2024/08/01/world/russia-prisoner-swap-us
 How Hamas Leader Ismail Haniyeh Was Killed in Iran
 https://www.nytimes.com/2024/08/01/world/middleeast/how-hamas-leader-haniyeh-killed-iran-bomb.html
 Israel Claims Killing of Militant Leader as Funerals Are Held for 2 Others
 https://www.nytimes.com/live/2024/08/01/world/israel-hamas-iran-haniyeh-gaza
 Fears of Wider Mideast Conflict Deepen, With U.S. Seen as ‘Not in Control’
 https://www.nytimes.com/2024/08/01/world/middleeast/middle-east-israel-iran-hezbollah.html
 How Track’s Athletics Integrity Unit Catches Doping Cheats
 https://www.nytimes.com/2024/08/01/sports/olympics-track-field-doping.html

$ date -R -u
Fri, 02 Aug 2024 01:31:52 +0000

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEepVDgp5HXX04JrCNpyXLV8plyv4FAmasNwgACgkQpyXLV8pl
yv6QHg/+NX6y0ATKMG3P8eZ+EynkAYipv9pxjjDGLaYl69xS9fONYmp3LArejKH3
6rMAdIbWF7qG8JAcGvgzUoD0y16g0bqMA6fY0cLOL4mWJJ98NTfZXHPttRsv038g
/oZ931f1jRCaBNnWhtpwLeZivUE6rNjLJpZCsRP1E83NxBldlIZHFPnK19phhIGG
WBg6BOq05vCCoQcfVwFfOcdKzRtzOu+ihQ4HOzdhEcTcRD8kZobrcWD1xwzwz2VP
RZnSB8hgtaQT/OrWVOWud5JLtrTkZYoQV3IavgUw+M15xkquQ1vn8PGL4aCF0G+j
ZYBDq8RgWc7bFbo8DssmJRZUJ/aZFUQXdnmRrmJWwht7Wx4oY+GoJVSmysUwUNrY
caXQcBPXDHWgCO2s8f0xMw00cRB83I8A6LcXW/yAmS0rEMHK/ahuJ68xzELRhJ03
24X/DDJsT6aQX5XBCKO5gRoMjctwZozmPuZEUVmw5BHfFrchZB5KMWQvCiRxd7C5
Bqh5DCrh5MBkNIgBa3133hHiYPd70QAJvrZ3mVWmj+BT2eqYg5y5yeQIYCgspeM9
Euv2ACTicU9YDTBr8AXph5Hjn2S6at0Pd1i+Jh2MqE+mzBrfnNbnq3+TMBuz/xYk
2m4OzlV8hP8WX7nHeCJ6sqFB3j9iui4gPS3yxrMkZcEHlkVf93w=
=nCHH
-----END PGP SIGNATURE-----
