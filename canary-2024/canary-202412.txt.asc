-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

WARRANT CANARY
====================================
Last updated       : 2024/12/02
Next update before : 2025/01/04
====================================
What is a warrant canary: https://en.wikipedia.org/wiki/Warrant_canary
====================================

Issued for 2024/12.

Don't just trust the contents of this file blindly! Verify the
digital signatures! Also, make sure the keys are correct!

Signers
~~~~~~~~~~~~~

Outvi V: 0xFF62255C1087F21C
 (Main key: 0xA725CB57CA65CAFE)

Statements
~~~~~~~~~~~~~

1. All our infrastructure is in our control, the integrity of our
entire system is sound.

2. We have not been compromised or suffered a recent data breach,
to our best knowledge.

3. We have not disclosed any private encryption keys.

4. We have not been forced to modify our system to allow access or
information leakage to a third party.

5. We haven't received any specific orders, requests or recommendations
from any authorities, whether formal or informal.

6. We have not received any court orders, gag orders, or other similar
orders from the government of the People's Republic of China.

7. We have not received any government subpoenas.

8. Our personal safety and security is not threatened.

9. We plan to publish the next of these canary statements before 2025/01/04.
Special note should be taken if no new canary is published by that time
or if the list of statements changes without plausible explanation.

10. We still have access to the "real" Internet, e.g. New York Times.

Special Announcements
~~~~~~~~~~~~~~~~~~~~~~~~

旅に出たい

Proof of Freshness
~~~~~~~~~~~~~~~~~~~~

$ rsstail -n5 -u https://www.telegraph.co.uk/news/rss.xml -l -N -1
 Ukraine: The Latest - the Telegraph’s most popular ever podcast
 https://www.telegraph.co.uk/news/2022/03/02/russia-ukraine-war-listen-daily-podcast/
 Storm Bert: Do not travel, public warned as disruption continues
 https://www.telegraph.co.uk/news/2024/11/25/storm-bert-latest-news/
 Fourth person confirmed dead in Storm Bert as body found in search for missing dog walker
 https://www.telegraph.co.uk/news/2024/11/24/storm-bert-latest-news-traffic-floods-roads/
 Harshita Brella’s parents ‘want justice’ for murdered daughter
 https://www.telegraph.co.uk/news/2024/11/18/harshita-brellas-neighbour-heard-banging-row-before-death/
 Violinist warns of Edinburgh International Festival cuts after SNP leaves funding in limbo
 https://www.telegraph.co.uk/news/2024/11/14/edinburgh-international-festival-snp-scottish-budget/

$ rsstail -n5 -u https://rss.nytimes.com/services/xml/rss/nyt/World.xml -l -N -1
 Mexican Cartels Lure Chemistry Students to Make Fentanyl
 https://www.nytimes.com/2024/12/01/world/americas/mexico-fentanyl-chemistry-students.html
 80 Years After Killings, Senegal Wants the Facts From France
 https://www.nytimes.com/2024/12/01/world/africa/france-senegal-thiaroye-massacre.html
 Angola, the U.S. and a Slavery Connection Few Talk About
 https://www.nytimes.com/2024/12/01/world/africa/angola-biden-slavery.html
 Syria Rebels Take Aleppo Airport and Attack Hama, Officials and a Monitor Say
 https://www.nytimes.com/2024/12/01/world/middleeast/syria-war-aleppo-rebels-government.html
 Former Defense Minister Accuses Israel of Committing War Crimes in Gaza
 https://www.nytimes.com/2024/12/01/world/middleeast/israel-gaza-yaalon.html

$ date -R -u
Mon, 02 Dec 2024 01:58:33 +0000

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEepVDgp5HXX04JrCNpyXLV8plyv4FAmdNFEoACgkQpyXLV8pl
yv74AQ//bP9RnRgH8WZ/2J86gVNWROwURzJRC4Qp7ImQIU/wx5qXvQzTVnq5aY7T
wnkfSl0OdC8fERr/svS25W0lxn967yvJWn08puGLaxixyCByNcnoLzQwkcXE2rJW
I1dn6Al42XpCLPJZlI1z9XPV4WSNfU9nB1V6bpoGAO/fsNQkhYX9CdU+0dLTdbK0
vfLInsA/nh+l+tIpbWPugFwJbi9lCzk1mg34KXNoS/hp9WY9Ez5glCSca8OpRx4W
d8pKMba5/fYKn4P1782T/KJ35kMPTF6NUv4qZlYQpDhKbtzEscCe0W69qqAvhM4a
6PCB/HeDmeb+pW6t4OZg7zuQs+WCenv5RgC2cjr1mDqSB/O4cj2m3rhQKPFBStw+
ixl+m1y2pxDmb58xWOlLN6TKimgu56lX4ZrvvY8k+KnHHiwAQ+sChh3zVp14t3RR
2P66FsNgK8IGJrkUuQ8K3bZAX6MF8H+RuD+TvrykLyCkyRVz5hM1TJFQYbiesElR
mcR6dKn4n4nlrSXGAXPOSnXhCD235n+xdSJSXMGBPH4WnuvIBnVXJH/AANiRp4om
kL7EowgSnFsZ8vqYCKOnFEHTlqQEzF6NG5FYAN9Jhy4rKFhRpOnFGFUgvwf2Je6S
Kp/JE+f/nayjziD5rqK0UQGKiy4c5bO+zVF+H2dvKS7FreUnw2g=
=u/BP
-----END PGP SIGNATURE-----
