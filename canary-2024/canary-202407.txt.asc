-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

WARRANT CANARY
====================================
Last updated       : 2024/07/02
Next update before : 2024/08/04
====================================
What is a warrant canary: https://en.wikipedia.org/wiki/Warrant_canary
====================================

Issued for 2024/07.

Don't just trust the contents of this file blindly! Verify the
digital signatures! Also, make sure the keys are correct!

Signers
~~~~~~~~~~~~~

Outvi V: 0xFF62255C1087F21C
 (Main key: 0xA725CB57CA65CAFE)

Statements
~~~~~~~~~~~~~

1. All our infrastructure is in our control, the integrity of our
entire system is sound.

2. We have not been compromised or suffered a recent data breach,
to our best knowledge.

3. We have not disclosed any private encryption keys.

4. We have not been forced to modify our system to allow access or
information leakage to a third party.

5. We haven't received any specific orders, requests or recommendations
from any authorities, whether formal or informal.

6. We have not received any court orders, gag orders, or other similar
orders from the government of the People's Republic of China.

7. We have not received any government subpoenas.

8. Our personal safety and security is not threatened.

9. We plan to publish the next of these canary statements before 2024/08/04.
Special note should be taken if no new canary is published by that time
or if the list of statements changes without plausible explanation.

10. We still have access to the "real" Internet, e.g. New York Times.

Special Announcements
~~~~~~~~~~~~~~~~~~~~~~~~



Proof of Freshness
~~~~~~~~~~~~~~~~~~~~

$ rsstail -n5 -u https://www.telegraph.co.uk/news/rss.xml -l -N -1
 Monday evening news briefing: Trump may be able to claim immunity over Jan 6, Supreme Court rules
 https://www.telegraph.co.uk/news/2024/07/01/monday-evening-news-briefing-trump-jan-6-supreme-court/
 Friday evening news briefing: Biden under pressure to quit – but advisers defiant
 https://www.telegraph.co.uk/news/2024/06/28/friday-evening-news-briefing-biden-under-pressure-to-quit/
 When is the US election? Everything you need to know about the 2024 race
 https://www.telegraph.co.uk/news/0/us-presidential-election-2024-when-dates-candidates-voting/
 Thursday evening news briefing: Labour to shut loophole in VAT raid on private school fees
 https://www.telegraph.co.uk/news/2024/06/27/thursday-evening-news-briefing-labour-to-shut-vat-loophole/
 Wednesday evening news briefing: The real cost of Labour’s net zero plans
 https://www.telegraph.co.uk/news/2024/06/26/wednesday-evening-news-briefing-cost-labour-net-zero-plans/

$ rsstail -n5 -u https://rss.nytimes.com/services/xml/rss/nyt/World.xml -l -N -1
 Middle East Crisis: Israeli Military Orders Evacuations in Southeastern Gaza
 https://www.nytimes.com/live/2024/07/01/world/israel-gaza-war-hamas
 France’s Snap Election: Here Is What’s at Stake
 https://www.nytimes.com/2024/07/01/world/europe/france-election-national-rally.html
 The Center Collapses in France, Leaving Macron Marooned
 https://www.nytimes.com/2024/07/01/world/europe/macron-election-national-rally.html
 Ukraine Says It Foiled Russian Plot Echoing String of Coup Bids
 https://www.nytimes.com/2024/07/01/world/europe/ukraine-coup-plot.html
 France’s Far Right Scores Big in First Round of Elections, Polling Suggests
 https://www.nytimes.com/2024/06/30/world/europe/france-elections.html

$ date -R -u
Tue, 02 Jul 2024 02:04:39 +0000

-----BEGIN PGP SIGNATURE-----

iQIzBAEBCAAdFiEEepVDgp5HXX04JrCNpyXLV8plyv4FAmaDYDcACgkQpyXLV8pl
yv79fBAAuLPjUxCvkMkbcVYedfDIaN/hr3PVFU/X8T/Ysc66HL/IYtVFMOqOQA1l
D7iCj5jkUuIHL725eCxnKXTzwhWJ9oxsgKGL7O6ycXjsv+bAKHvgTrAuWUT53kQ/
sgiPldgi56DjFU2kE/QsX/siUz9g9dm63h8/85gs83AGcLq4CNxGzus5jX0/hwv8
8wIWF3r98VpWb97f+3ZXeprHoV4mQ+CVhSclyRrniZV1HapYMIWzUlvk3yAfnxMl
ZC3mRxM1Vga1ElMoeLZZkc629FhrdT1uoIP+Earw4HIe8eyPecjejZmRs/Zhuv+Y
2UslpY6pLcRy/eQ3Ea2aF1wixhbgWOh47J9Dqh5gb/oypCbxegkitx+dGiWjRQ7Z
7vxKXXVlmL/UQCWCnM/EqfAsa+hsDjC375apCkb7H5mlXviyyVTuWEuOP4dG1RoI
ciAzVa6PJrIxNgSCsMcBaECnmaB81aV5J+27j7j7iLBTY/X5OBQ6GnFnlZGW09Nm
W4DVO7lFCseCbAC4N64U3C7NQ+88NJJHthOnxPoexP3D5J1vVuVnfJ+KIXhng21a
F05aYhTSd4Zp5rUMVtrVWeSAuM0vjZ0lEgDlsdkGJJKYFlXTbbpgwjoQV7VRjv0r
A+scHFMVUlNRvuOYCU3lXTB1Z1aL323m4zWHHalLMeCtx9PKuHM=
=h0cE
-----END PGP SIGNATURE-----
